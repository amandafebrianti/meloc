package id.ac.stiki.doleno.umkmchecker.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.stiki.doleno.umkmchecker.Model.BusinessModel;
import id.ac.stiki.doleno.umkmchecker.R;

public class BusinessAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BusinessModel> businesses;
    private Context mContext;
    private static onClickListener onClickListener;

    public static class MyViewHolderImage extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        @BindView(R.id.imgBusiness)
        ImageView imgBusiness;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtLatLong)
        TextView txtLatLong;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        @BindView(R.id.txtCategory)
        TextView txtCategory;
        @BindView(R.id.linearLayout)
        LinearLayout linearLayout;


        public MyViewHolderImage(View view){
            super(view);
            ButterKnife.bind(this, view);
            view.setOnLongClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {
            onClickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }

        @Override
        public void onClick(View v) {
            onClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(onClickListener onClickListener) {
        BusinessAdapter.onClickListener = onClickListener;
    }

    public BusinessAdapter(Context context, List<BusinessModel> messageList){
        this.businesses = messageList;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.business_layout, parent, false);
        return new MyViewHolderImage(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BusinessModel business = this.businesses.get(position);

        final MyViewHolderImage myViewHolder = (MyViewHolderImage) holder;

        String latLong = "(" + business.getLatitude() + ", " + business.getLongitude() + ")";

        myViewHolder.txtName.setText(business.getName());
        myViewHolder.txtLatLong.setText(latLong);
        myViewHolder.txtAddress.setText(business.getAddress());
        myViewHolder.txtCategory.setText(business.getCategory());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        final Bitmap bitmap = BitmapFactory.decodeFile(business.getPicURI(),options);
        myViewHolder.imgBusiness.setVisibility(ImageView.VISIBLE);
        myViewHolder.imgBusiness.setImageBitmap(bitmap);
        myViewHolder.imgBusiness.requestLayout();

        myViewHolder.linearLayout.requestLayout();
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)myViewHolder.linearLayout.getLayoutParams();
        myViewHolder.linearLayout.setLayoutParams(params);
    }

    @Override
    public int getItemCount() { return businesses == null ? 0 : businesses.size(); }

    public interface onClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}
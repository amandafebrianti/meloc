package id.ac.stiki.doleno.umkmchecker.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ac.stiki.doleno.umkmchecker.GPSTracker.GPSTracker;
import id.ac.stiki.doleno.umkmchecker.Model.BusinessModel;
import id.ac.stiki.doleno.umkmchecker.R;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class AddBusinessActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {

    @BindView(R.id.txtLatitude)
    EditText txtLatitude;
    @BindView(R.id.txtName)
    EditText txtName;
    @BindView(R.id.txtLongitude)
    EditText txtLongitude;
    @BindView(R.id.txtAddress)
    EditText txtAddress;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnCamera)
    ImageButton btnCamera;
    @BindView(R.id.spnCategory)
    Spinner spnCategory;
    @BindView(R.id.txtDescription)
    EditText txtDescription;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    private static final int CAMERA_PIC_REQUEST = 1337;
    private static final int CAMERA_PERMISSION_CODE = 2456;
    private static final int CAMERA_AND_STORAGE_PERMISSION_CODE = 2480;

    private static final String IMAGE_DIRECTORY_NAME = "UMKM";

    private GPSTracker gps;
    private File imageFile; // file url to store image

    private String[] arrayCategory = new String[]{
            "Cafe", "Co Working Space", "Mall"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business);
        ButterKnife.bind(this);

        gps = new GPSTracker(AddBusinessActivity.this);
        initSpinner();
        if (getIntent().getExtras() != null) {
            enabledEditText(false);
            BusinessModel business = (BusinessModel) getIntent().getParcelableExtra("business");
            fillBusinessText(business);
        } else {
            toolbarTitle.setText("Add New Place");
            enabledEditText(true);
        }

        permissionChecker();
    }

    private void initSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrayCategory);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCategory.setAdapter(adapter);
    }

    private void enabledEditText(boolean isEnabled) {
        txtName.setEnabled(isEnabled);
        spnCategory.setEnabled(isEnabled);
        txtDescription.setEnabled(isEnabled);
        txtAddress.setEnabled(isEnabled);
        txtLatitude.setEnabled(isEnabled);
        txtLongitude.setEnabled(isEnabled);
        btnCamera.setEnabled(isEnabled);

        if (isEnabled)
            btnSave.setVisibility(View.VISIBLE);
        else
            btnSave.setVisibility(View.GONE);
    }

    private void fillBusinessText(BusinessModel business) {
        toolbarTitle.setText(business.getName());
        txtName.setText(business.getName());
        txtDescription.setText(business.getDescription());
        txtAddress.setText(business.getAddress());
        txtLatitude.setText(business.getLatitude());
        txtLongitude.setText(business.getLongitude());

        int spinnerPosition = Arrays.asList(arrayCategory).indexOf(business.getCategory());
        spnCategory.setSelection(spinnerPosition);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        final Bitmap bitmap = BitmapFactory.decodeFile(business.getPicURI(), options);
        btnCamera.setVisibility(ImageView.VISIBLE);
        btnCamera.setImageBitmap(bitmap);
    }

    private void permissionChecker() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                !EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            EasyPermissions.requestPermissions(
                    this, "This apps needs write access to your storage and access camera",
                    CAMERA_AND_STORAGE_PERMISSION_CODE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        }
    }

    @OnClick({R.id.btnSave, R.id.btnCamera})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                saveBusiness();
                break;
            case R.id.btnCamera:
                captureImage();
                break;
        }
    }

    private void saveBusiness() {
        BusinessModel result = new BusinessModel(txtName.getText().toString(),
                txtLatitude.getText().toString(), txtLongitude.getText().toString(),
                imageFile.getAbsolutePath(), txtAddress.getText().toString(),
                txtDescription.getText().toString(), spnCategory.getSelectedItem().toString());

        Intent returnIntent = new Intent();
        returnIntent.putExtra("business", result);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) != null) {
            imageFile = getOutputMediaFile();
            if (imageFile != null) {
                Uri photoURI = getOutputMediaFileUri();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, CAMERA_PIC_REQUEST);
            }
        }
    }

    public Uri getOutputMediaFileUri() {
        return FileProvider.getUriForFile(
                AddBusinessActivity.this,
                AddBusinessActivity.this.getApplicationContext()
                        .getPackageName() + ".provider", imageFile);
    }

    /*
     * returning image / video
     */
    private File getOutputMediaFile() {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == RESULT_OK) {
                previewCapturedImage();
                if (gps.canGetLocation()) {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    txtLatitude.setText("" + latitude);
                    txtLongitude.setText("" + longitude);
                    txtAddress.setText(getAddress(latitude, longitude));
                } else {
                    Toast.makeText(getApplicationContext(), "Cant get your location, please enable your gps first", Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "Cancelled", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Error!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void previewCapturedImage() {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(),
                    options);

            btnCamera.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private String getAddress(double latitude, double longitude) {
        String result = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();

            result = address + ", " + city + ", " + state + ", " + country;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }
}

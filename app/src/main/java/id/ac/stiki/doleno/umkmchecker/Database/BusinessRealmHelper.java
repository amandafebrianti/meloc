package id.ac.stiki.doleno.umkmchecker.Database;

import android.util.Log;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmResults;
import id.ac.stiki.doleno.umkmchecker.Model.BusinessModel;

public class BusinessRealmHelper {
    private static final String TAG = "BusinessRealmHelper";

    private Realm realm;

    public BusinessRealmHelper(Realm mRealm) {
        realm = mRealm;
    }

    public void addBusiness(BusinessModel business) {
        BusinessRealmObject listObj = new BusinessRealmObject();
        listObj.setID(getBusinessId());
        listObj.setName(business.getName());
        listObj.setLatitude(business.getLatitude());
        listObj.setLongitude(business.getLongitude());
        listObj.setPicURI(business.getPicURI());
        listObj.setAddress(business.getAddress());
        listObj.setCategory(business.getCategory());
        listObj.setDescription(business.getDescription());

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(listObj);
        realm.commitTransaction();
    }

    public void deleteBusiness(BusinessModel business) {
        realm.beginTransaction();
        RealmResults<BusinessRealmObject> result = realm.where(BusinessRealmObject.class).equalTo("ID", business.getID()).findAll();
        result.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public ArrayList<BusinessModel> getListBusiness() {
        ArrayList<BusinessModel> data = new ArrayList<>();

        RealmResults<BusinessRealmObject> rCall = realm.where(BusinessRealmObject.class).findAll();
        if(rCall.size() > 0)
            showLog("Size : " + rCall.size());

        for (int i = 0; i < rCall.size(); i++) {
            data.add(new BusinessModel(rCall.get(i)));
        }

        return data;
    }

    public int getBusinessId(){
        RealmResults<BusinessRealmObject> rCall = realm.where(BusinessRealmObject.class).findAll();

        return rCall.size() + 1;
    }

    private void showLog(String s) {
        Log.d(TAG, s);
    }
}

package id.ac.stiki.doleno.umkmchecker.Model;

import android.os.Parcel;
import android.os.Parcelable;

import id.ac.stiki.doleno.umkmchecker.Database.BusinessRealmObject;

public class BusinessModel implements Parcelable {
    private int ID;
    private String Name;
    private String Latitude;
    private String Longitude;
    private String PicURI;
    private String Address;
    private String Category;
    private String Description;

    public BusinessModel() {
        this.Name = "";
        this.Latitude = "";
        this.Longitude = "";
        this.PicURI = "";
        this.Address = "";
        this.Category = "";
        this.Description = "";
    }

    public BusinessModel(String name, String latitude, String longitude, String picURI, String address, String description, String category){
        this.Name = name;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.PicURI = picURI;
        this.Address = address;
        this.Description = description;
        this.Category = category;
    }

    public BusinessModel(BusinessRealmObject obj){
        this.ID = obj.getID();
        this.Name = obj.getName();
        this.Latitude = obj.getLatitude();
        this.Longitude = obj.getLongitude();
        this.PicURI = obj.getPicURI();
        this.Address = obj.getAddress();
        this.Description = obj.getDescription();
        this.Category = obj.getCategory();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getPicURI() {
        return PicURI;
    }

    public void setPicURI(String picURI) {
        PicURI = picURI;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(Latitude);
        dest.writeString(Longitude);
        dest.writeString(PicURI);
        dest.writeString(Address);
        dest.writeString(Category);
        dest.writeString(Description);
    }

    public static final Parcelable.Creator<BusinessModel> CREATOR
            = new Parcelable.Creator<BusinessModel>() {
        public BusinessModel createFromParcel(Parcel in) {
            return new BusinessModel(in);
        }

        public BusinessModel[] newArray(int size) {
            return new BusinessModel[size];
        }
    };

    private BusinessModel(Parcel in) {
        Name = in.readString();
        Latitude = in.readString();
        Longitude = in.readString();
        PicURI = in.readString();
        Address = in.readString();
        Category = in.readString();
        Description = in.readString();
    }
}

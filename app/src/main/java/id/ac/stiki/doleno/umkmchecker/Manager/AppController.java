package id.ac.stiki.doleno.umkmchecker.Manager;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;

public class AppController extends Application {

    private static Context context;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Init Realm
        Realm.init(this);
    }

    public static Context getContext(){
        return context;
    }
}
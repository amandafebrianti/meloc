package id.ac.stiki.doleno.umkmchecker.Database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BusinessRealmObject extends RealmObject {
    @PrimaryKey
    private int ID;
    private String Name;
    private String Latitude;
    private String Longitude;
    private String PicURI;
    private String Address;
    private String Category;
    private String Description;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getPicURI() {
        return PicURI;
    }

    public void setPicURI(String picURI) {
        PicURI = picURI;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

}

package id.ac.stiki.doleno.umkmchecker;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import id.ac.stiki.doleno.umkmchecker.Activity.AddBusinessActivity;
import id.ac.stiki.doleno.umkmchecker.Adapter.BusinessAdapter;
import id.ac.stiki.doleno.umkmchecker.Database.BusinessRealmHelper;
import id.ac.stiki.doleno.umkmchecker.Model.BusinessModel;
import id.ac.stiki.doleno.umkmchecker.R;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, EasyPermissions.RationaleCallbacks {
    @BindView(R.id.empty_list)
    LinearLayout emptyList;
    @BindView(R.id.recyclerBusiness)
    RecyclerView recyclerBusiness;
    @BindView(R.id.btnAdd)
    FloatingActionButton btnAdd;

    private static final int ADD_BUSINESS_REQ_CODE = 3;
    private static final int LOCATION_AND_STORAGE_PERMISSION_CODE = 2341;

    private BusinessAdapter mAdapter;
    private List<BusinessModel> businessList;
    private BusinessRealmHelper helper;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        helper = new BusinessRealmHelper(realm);

        businessList = helper.getListBusiness();

        //init recyclerview
        initRecyclerView();

        updateLayout();

        permissionChecker();
    }

    private void updateLayout() {
        if (businessList.isEmpty()) {
            emptyList.setVisibility(View.VISIBLE);
            recyclerBusiness.setVisibility(View.GONE);
        } else {
            emptyList.setVisibility(View.GONE);
            recyclerBusiness.setVisibility(View.VISIBLE);
        }
    }

    private void initRecyclerView() {
        mAdapter = new BusinessAdapter(MainActivity.this, businessList);
        mAdapter.setOnItemClickListener(new BusinessAdapter.onClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                showBusiness(businessList.get(position));
            }

            @Override
            public void onItemLongClick(int position, View v) {
                showDeleteAlert(position);
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerBusiness.setLayoutManager(mLayoutManager);
        recyclerBusiness.setNestedScrollingEnabled(true);
        recyclerBusiness.setVerticalScrollBarEnabled(false);
        recyclerBusiness.setAdapter(mAdapter);
    }

    private void showBusiness(BusinessModel business) {
        Intent i = new Intent(this, AddBusinessActivity.class);
        i.putExtra("business", business);
        startActivity(i);
    }

    private void showDeleteAlert(int position) {
        BusinessModel business = businessList.get(position);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("Delete");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure want to delete '" + business.getName() + "' ?");

        // On pressing the Settings button.
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                helper.deleteBusiness(business);
                refreshView();
            }
        });

        // On pressing the cancel button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void refreshView() {
        businessList.clear();
        businessList.addAll(helper.getListBusiness());
        mAdapter.notifyDataSetChanged();
        recyclerBusiness.smoothScrollToPosition(businessList.size());
        updateLayout();
    }

    private void permissionChecker() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                !EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            EasyPermissions.requestPermissions(
                    this, "This apps needs access to read your storage and location",
                    LOCATION_AND_STORAGE_PERMISSION_CODE,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @OnClick(R.id.btnAdd)
    public void onViewClicked() {
        Intent i = new Intent(this, AddBusinessActivity.class);
        startActivityForResult(i, ADD_BUSINESS_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_BUSINESS_REQ_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                BusinessModel result = (BusinessModel) data.getParcelableExtra("business");
                helper.addBusiness(result);
                refreshView();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {

    }

    @Override
    public void onRationaleDenied(int requestCode) {

    }
}
